# ryn

[![Crates.io](https://img.shields.io/crates/v/ryn.svg)](https://crates.io/crates/ryn)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/ryn)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/ryn/-/raw/master/LICENSE)

Simple Multi-User Chat cli. 

this project is from `https://github.com/tsoding/4at` and new functions and simlify useage for improve capability.

## Quick Start

### prepare in linux
```sh
apt install libxcb-shape0-dev libxcb-xfixes0-dev
```

### install

```
cargo install ryn
```
### Server

```sh
ryn_server
```

Upon running the server creates `./TOKEN` where the Authentication Token is located. You will needed to connect to the Server via the Client.

### Client

```sh
ryn_client
```
```
/connect <ip> <token> - Connect server by <ip>, do't need port default port is 8081 , authorization <token>, /connect 172.20.10.3 fdac                                                                        
/c <ip> <token> - Connect server by <ip>, do't need port default port is 8081,authorization <token>,/c 172.20.10.3 fdac                                                                                       
/disconnect - Disconnect from the server you are currently connected to                                                                                                                                       
/d - Disconnect from the server you are currently connected to                                                                                                                                                
/send_clip - send clipboard to the server you are currently connected                                                                                                                                         
/sc - send clipboard to the server you are currently connected                                                                                                                                                
/send_file <file_path> - send file to the server you are currently connected ,file_path is absolute path                                                                                                                            
/sf <file_path> - send file to the server you are currently connected ,file_path is absolute path
/quit - Close the chat                                                                                                                                                                                        
/q - Close the chat                                                                                                                                                                                           
/help [command] - Print help                                                                           
```



In the prompt of the Client

```console
> /c <server ip> <token>
```
